#include <boost/any.hpp>
#include <iostream>

extern "C" boost::any& add_some(boost::any &data, boost::any &params)
{
    int a, b;
    try
    {
        a = boost::any_cast<int>(data);
        b = boost::any_cast<int>(params);
    }
    catch (boost::bad_any_cast &e)
    {
        std::cerr << e.what() << std::endl;
    }

    data = a + b;
    return data;
}