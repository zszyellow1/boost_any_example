#include <opencv2/opencv.hpp>
#include <vector>
#include "utils.hpp"
#include "tbb/parallel_for_each.h"
#include "tbb/task_scheduler_init.h"
#include <dlfcn.h>

#define LIB_CACULATE_PATH "../shared_algorithms/libcalc.dylib"
typedef boost::any& (*CALC_FUNC)(boost::any&, boost::any&);

CALC_FUNC find_algorithm(const char* func_name)
{
    void *handle;
    char *error;
    CALC_FUNC calc_func = NULL;

    handle = dlopen(LIB_CACULATE_PATH, RTLD_LAZY);
    if (!handle) 
    {
        std::cerr << dlerror() << std::endl;
        exit(EXIT_FAILURE);
    }
    dlerror();
    *(void **) (&calc_func) = dlsym(handle, func_name);
    if ((error = dlerror()) != NULL)  
    {
        std::cerr << error << std::endl;
        exit(EXIT_FAILURE);
    }
    return calc_func;
}

struct mytask 
{
    mytask(int n):_n(n) {}
    int _n;
    void operator()() 
    {
        std::string func_name("add_some");
        const char *func = func_name.c_str();
        CALC_FUNC calc = find_algorithm(func);
        boost::any a = _n;
        boost::any b = 1;
        boost::any result = (*calc)(a, b);
        int tmp = anyToInt(result);
        std::cout << tmp << " ";
    }
};

template <typename T> 
struct invoker 
{
    void operator()(T& it) const { it(); }
};

int main()
{
    // supporting int
    boost::any a = 1;
    getType(a);

    // supporting double
    a = 3.14;
    getType(a);

    // supporting Mat in cv
    cv::Mat M(2,2, CV_8UC3, cv::Scalar(0,0,255));
    a = M;
    getType(a);

    // supporting std::vector
    a = std::vector<int>(2, 0);
    getType(a);

    // cast to specific type
    try
    {
        std::string tmp = boost::any_cast<std::string>(a);
        std::cout << tmp << std::endl;
    }
    catch (boost::bad_any_cast &e)
    {
        std::cerr << e.what() << std::endl;
    }

    // test combined with tbb
    tbb::task_scheduler_init init;
    std::vector<mytask> tasks;
    for (int i = 0; i < 100; ++i)
        tasks.push_back(mytask(i));

    tbb::parallel_for_each(tasks.begin(), tasks.end(), invoker<mytask>());
    std::cerr << std::endl;

    a = 1;
    int int_a = boost::any_cast<int>(a);
    std::cout << int_a << std::endl;
    return int_a;
}