#include <boost/any.hpp>
#include <string>
#include <cxxabi.h>
#include <typeinfo>
#include <iostream>
#include <memory>
#include <cstdlib>

std::string demangle(const char* mangled);
void getType(boost::any a);
int anyToInt(boost::any a);
