#include "utils.hpp"

std::string demangle(const char* mangled)
{
    int status;
    std::unique_ptr<char[], void (*)(void*)> result(abi::__cxa_demangle(mangled, 0, 0, &status), std::free);
    return result.get() ? std::string(result.get()) : "error occurred";
}

void getType(boost::any a)
{
    if (!a.empty())
    {
        const std::type_info &t = a.type();
        std::cout << demangle(t.name()) << std::endl;
    }
}

int anyToInt(boost::any a)
{
    int tmp;
    try
    {
        tmp = boost::any_cast<int>(a);
    }
    catch (boost::bad_any_cast &e)
    {
        std::cerr << e.what() << std::endl;
    }
    return tmp;
}